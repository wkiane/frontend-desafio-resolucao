# Convenia

## Avaliação Técnica - Frontend

Uma aplicação para realizar calculo de gorjetas baseado no preço da refeição porcentagem da gorjeta e preço por pessoa, também é feita a conversão do preço para reais.

Na aplicação foram usados os pacotes vuex para gerenciamento de estado, sass para estilização e vite para geração e build da aplicação.

## Como rodar o projeto
- Ir ao diretório da aplicação ```cd letip```
- Rodar ```npm i``` para instalar as dependências
- Rodar ```npm run dev``` para iniciar o servidor
 
# Importante
Para conversão da moeda para reais importante iniciar a API GraphQL fornecida no teste e criar um .env com a url para a api assim como no .env.example. 