import { convertCurrency } from "../../services/convert_currency"
import { makeCalc } from "../../utils/make_calc"

export default {
    state: {
        currency: '',
        people: 2,
        tip: 10,
        tipCalc: 0,
        rateToBRL: 0,
        mealPrice: 0,
        totalPrice: 0,
        perPerson: 0,
        inBRL: 0
    },
    mutations: {
        setCurrency(state, data) {
            state.currency = data
        },
        setPeople(state, data) {
            state.people = data
        },
        setTip(state, data) {
            state.tip = data
        },
        setMealPrice(state, data) {
            state.mealPrice = data
        },
        setInBRL(state, data) {
            state.inBRL = data
        }
    },
    actions: {
        toogleCurrency({ commit, dispatch }, data) {
            commit('setCurrency', data)
            dispatch('updateInBRL');
        },
        async updateInBRL({ commit, state, getters }) {
            const resp = await convertCurrency(state.currency, "BRL");
            const _inBRL =(getters.tipCalculated.totalPrice / resp.rate).toFixed(2)
            commit('setInBRL', parseFloat(_inBRL))
        },
        updatePeople({ commit }, data) {
            commit('setPeople', parseFloat(data))
        },
        updateTip({ commit, dispatch }, data) {
            commit('setTip', parseFloat(data))
            dispatch('updateInBRL');
        },
        updateMealPrice({ commit, dispatch }, data) {
            commit('setMealPrice', data ? parseFloat(data) : 0)
            dispatch('updateInBRL');
        },
    },
    getters: {
        currency(state) {
            return state.currency
        },
        tip(state) {
            return state.tip
        },
        people(state) {
            return state.people
        },
        mealPrice(state) {
            return state.mealPrice
        },
        tipCalculated(state) {
            return makeCalc({ mealPrice: state.mealPrice, quantityPeople: state.people, tip:  state.tip })
        },
        inBRL(state) {
            return state.inBRL
        },
    }
}