import { createStore } from 'vuex'
import cal from './modules/calc'

export default createStore({
    modules: {
        cal
    }
})
