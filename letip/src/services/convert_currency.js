export async function convertCurrency(baseCurrency, convertCurrencies) {
    const endpoint = import.meta.env.VITE_CURRENCY_CONVERTER_URL + "/graphql";

    const graphqlQuery = {
        query: `query currencyConversion($baseCurrency: String!, $convertCurrencies: [String]!){
            currencyConversion(baseCurrency: $baseCurrency, convertCurrencies: $convertCurrencies){
                conversions { rate }
            }
        }`,
        variables: JSON.stringify({ baseCurrency, convertCurrencies }
        )
    };
    const queryParams = '?' + new URLSearchParams(graphqlQuery).toString()

    const options = {
        "method": "GET",
        "headers": {"content-type": "application/json"}
    };

    const response = await window.fetch(endpoint + queryParams, options);
    const data = await response.json();

    if (data.errors) {
        alert('Não foi possível converter fazer a conversão para Reais')
        return {rate: 0};
    }
    const [convertion] = data.data.currencyConversion.conversions;
    return convertion;
}