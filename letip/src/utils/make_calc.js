export function makeCalc({ mealPrice, quantityPeople, tip }) {
    if (mealPrice <= 0 || quantityPeople <= 0 || tip <= 0) {
        return {
            totalPrice: 0,
            tipCalc: 0,
            perPerson: 0
        }
    }
    
    const tipCalc = parseFloat(((mealPrice * tip) / 100).toFixed(2));
    const totalPrice = parseFloat((mealPrice + tipCalc).toFixed(2));
    const perPerson = parseFloat((totalPrice / quantityPeople).toFixed(2));
    
    return {
        totalPrice,
        tipCalc,
        perPerson
    }
}